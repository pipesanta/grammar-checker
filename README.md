
Proyecto para validar los tipos de gramaticas S, Q y LL1

SEGUNDA PRÁCTICA

LABORATORIO DE TEORÍA DE LENGUAJES 20192

&quot;Realizar un programa (en el lenguaje de su preferencia) que acepte como entrada una gramática
cualquiera, el programa deberá:
 - Identificar si es una gramática de la forma S, Q ó LL(1).
 - Si es alguna de las anteriores deberá construir el autómata de pila que reconozca el lenguaje
que genera dicha gramática.
- Si no es alguna de las anteriores deberá especificar por qué no.
 - Finalmente, independiente de si es o no S, Q ó LL(1), deberá dar la opción de terminar el
programa ó ingresar una nueva gramática.&quot;
- Además de construir el autómata de pila, éste deberá ser funcional, es decir, aceptar hileras de
entrada y decir si pertenecen o no al lenguaje cuyo reconocedor se construyó.

Notas:
 Las prácticas se desarrollan en equipos de máximo dos estudiantes.
 Prácticas iguales se califican con cero.
 No olvide manual técnico y manual de usuario si su programa tiene alguna característica especial.
 La práctica debe enviarse en formato  .zip y debe nombrarse con el siguiente formato
[Estudiante1][Estudiante2][practica2][TdeL].zip, y que en el correo de entrega se especifiquen los
integrantes con nombres y apellidos.

Fecha de entrega: 22/05/2020 Hasta las 23:59


---------------------------------------------------------------------------------------
Pasos para crear pratica 2 
## VISTA_PRINCIPAL
1. es una vista donde hay solo un cuadro de texto donde se le pide al usuario los estados no terminales separados por comas,
para agilizar la creacion de la gramatica
    Nota: Los no terminales que no se ingresen aca podran ser ingresados en la definición de la gramatica.
2. hay un boton de  "CONTINUAR" el cual llevara a la vista de <DEFINICION DE LA GRAMATICA\> crear la definicion de cada una de las producciones.
	
## DEFINICION_DE_LA_GRAMATICA
1. Hay un boton en la parte de arriba. que sirve para editar los no terminales.
2. cuando se esten llenando la definicion de cada produccion se pueden habilitar unos botones que represneten cada "NO TERMINAL" para crear la expresion muy rapido. De modo que cuando se de click sobre cada uno de los botones se agregen al input, sin necesitad de escribir todo el "<B\>".
3. al frente de cada produccion habrá un boton para eliminar la fila que representa la produccion
4. hay un boton en la parte inferior izquierdo "CANCELAR" cuando se presione saldra un dialogo de confirmacion avisando que si confirma se limpiara todos las procucciones, y asi se regresará a la VISTA_PRINCIPAL
5. Hay un boton de la parte inferior derecha "Validar gramatica". el cualal ser presionado nos va a llevar al una nueva vista "RESULTADOS" y estando allí, se realizará el proceso de reconocimiento si la gramatica ingresada es de los tipos  S, Q ó LL1.
5. Se mostrará por cada tipo de gramatica (S, Q y LL1), si la gramatica anteriormente ingresada es algun tipo de los que se vnaa validar. de los contrario se van especificar por que no lo es.
en caso de que si sea una expresion alida para cada uno de los tipos. se mostrar un icno de check en verde. de lo contrario una "X" en rojo.
#LOGICA
1. A partir de los datos ingresados convertirlos en tipo de dato <GRAMMAR>.
1. Como se va a validar si es de un tipo o del otro.. ?
	
	



#LOGICA_GENERAL
1. 
2. crear la clase Stack y sus metodos.
2. 
