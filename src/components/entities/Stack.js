'use strict'

class Stack {

    constructor(){
        /** @type {number} */
        this.top = 0;
        /** @type {Array} */
        this.items = [];
    }

    push(element){
        this.top = this.top +1;
        this.items.push(element);
        return;
    }
    
    isEmpty(){
        return this.top === 0;
    }

    print(){
        console.log(this);
    }

    search(element){
        return this.items.findIndex(e => e === element);
    }

    /**
     * 
     * @param {[String]} items 
     */
    replace(itemsToReplace){
        if(!Array.isArray(itemsToReplace)){
            throw "items argument must be an array of string elements"
        }

        this.items.pop();
        this.items.push(...itemsToReplace);

    }

    pop(){
        this.items.pop();
    }

    getTop(){
        return this.items[this.items.length-1];
    }


}

module.exports = Stack;