const Grammar = require("./Grammar");
const Tools = require("./Tools");
const Stack = require("./Stack");

const allowedTypes = ["SS", "Q", "LL(1)"];

class StackAutomata {

    constructor(grammar) {
        this.inputSymbols = [];
        this.stackSymbols = [];
        this.initialSetup = [];
        /** @type {Grammar} */
        this.grammar = grammar;
        this.transitions = {};
    }

    // cosntruye los simbolos de entrada, simbolos en pila y configuracion inicial de la pila
    buildAutomata() {
        if (!allowedTypes.includes(this.grammar.type)) {
            throw "Grammar must be an known type"
        }

        let noTerminalsToStackSymbols = [];

        // SIMBOLOS DE ENTRADA
        this.inputSymbols = this.grammar.productions
            .map(p => p.definitionItems)
            .reduce((acc, items) => {
                const newItems = [...new Set(items)].filter(i => !acc.includes(i) && Tools.isTerminal(i));
                acc.push(...newItems);
                return acc;
            }, []);
        this.inputSymbols.push("-|");

        // SIMBOLOS EN PILA
        noTerminalsToStackSymbols = this.grammar.productions.map(p => p.leftSide);


        const terminalsForStackSymbols = this.grammar.productions
            .map(p => p.definitionItems.slice(1))
            .reduce((acc, value) => {
                const terminalItems = value.filter(i => Tools.isTerminal(i) && !acc.includes(i));
                acc.push(...new Set(terminalItems));
                return acc;
            }, []);

        this.stackSymbols = [
            ...new Set(noTerminalsToStackSymbols),
            ...new Set(terminalsForStackSymbols),
            "▼"
        ]
        // configuracion inicial
        this.initialSetup = ["▼", this.grammar.productions[0].leftSide];



    }

    printInfo() {
        console.log({
            "SIMBOLOS_EN_PILA": JSON.stringify(this.stackSymbols),
            "SIMBOLOS_DE_ENTRADA": JSON.stringify(this.inputSymbols),
            "CONFIGURACION_INICIAL": JSON.stringify(this.initialSetup)
        });
    }

    buidTransitionObject() {

        if (!allowedTypes.includes(this.grammar.type)) {
            throw "Grammar must be an known type"
        }

        switch (this.grammar.type) {
            case "SS":
                this.stackSymbols.forEach(symbol => {
                    if (!this.transitions[symbol]) this.transitions[symbol] = {};
                    if (symbol === "▼") {
                        const handler = function () {
                            return "ACCEPT";
                        }
                        this.transitions[symbol]["-|"] = { text: "ACEPTAR", fn: handler };
                    }
                    if (Tools.isTerminal(symbol) && symbol !== "▼") {
                        const handler = function (stack) {
                            this.pop();
                        }
                        this.transitions[symbol][symbol] = { text: "Desapile, Avance", fn: handler };
                    }
                    // cuando son No terminales.
                    if (Tools.isNotTerminal(symbol) && !this.grammar.noTerminalsAvoidable.includes(symbol)  ) {
                                                
                        this.grammar.firstOnes[symbol].forEach(first => {
                            const that = this;

                            const handler = function (itemOnTop, justText = false) {
                                let text = "";
                                let alpha = [that.grammar.productions
                                    .find(p => (p.leftSide == itemOnTop && p.definitionItems[0] == first)).definitionItems.slice(1)]
                                alpha.reverse();

                                if (alpha.length > 0) {
                                    text = `Replace(${alpha}), Avance`;
                                    if (justText) return text;
                                    this.replace(alpha);
                                } else {
                                    text = "Desapile, Avance";
                                    if (justText) return text;
                                    this.pop();
                                }
                            }

                            let text = handler(symbol, true);

                            this.transitions[symbol][first] = { text: text, fn: handler };
                        })
                    }

                })
                break;

            case "Q": 
                this.stackSymbols.forEach(symbol => {
                    if (!this.transitions[symbol]) this.transitions[symbol] = {};
                    if (symbol === "▼") {
                        const handler = function () {
                            return "ACCEPT";
                        }
                        this.transitions[symbol]["-|"] = { text: "ACEPTAR", fn: handler };
                    }
                    if (Tools.isTerminal(symbol) && symbol !== "▼") {
                        const handler = function (stack) {
                            this.pop();
                        }
                        this.transitions[symbol][symbol] = { text: "Desapile, Avance", fn: handler };
                    }

                    // cuando son No terminales 
                    if (Tools.isNotTerminal(symbol)) {
                        this.grammar.firstOnes[symbol].forEach(first => {
                            const that = this;
                            const handler = function (itemOnTop, justText = false) {
                                let text = "";
                                let alpha = [...that.grammar.productions
                                    .find(p => (p.leftSide == itemOnTop && p.definitionItems[0] == first)).definitionItems.slice(1)]
                                alpha.reverse();

                                if (alpha.length > 0) {
                                    text = `Replace(${alpha}), Avance`;
                                    if (justText) return text;
                                    this.replace(alpha);
                                } else {
                                    text = "Desapile, Avance";
                                    if (justText) return text;
                                    this.pop();
                                }
                            }

                            let text = handler(symbol, true);

                            this.transitions[symbol][first] = { text: text, fn: handler };
                        })
                    }


                    const voidableDirectly = this.grammar.getAvoidableProductions()
                        .filter(obj => obj.production.leftSide === symbol)
                        .length > 0;

                    // cuando es No-terminales y es anulable.
                    if(Tools.isNotTerminal(symbol) && voidableDirectly){
                        this.grammar.nextOnes[symbol].forEach(nextOne => {
                            const that = this;
                            const handler = function (itemOnTop, justText = false) {
                                this.pop();
                                return "KEEP"
                            }
                            const text = "Desapile, Retenga";

                            this.transitions[symbol][nextOne] = { text, fn: handler };
                        })
                    }

                });
                break;
            case "LL(1)":

                this.stackSymbols.forEach(symbol => {
                    if (!this.transitions[symbol]) this.transitions[symbol] = {};
                    if (symbol === "▼") {
                        const handler = function () {
                            return "ACCEPT";
                        }
                        this.transitions[symbol]["-|"] = { text: "ACEPTAR", fn: handler };
                    }
                    if (Tools.isTerminal(symbol) && symbol !== "▼") {
                        const handler = function (stack) {
                            this.pop();
                        }
                        this.transitions[symbol][symbol] = { text: "Desapile, Avance", fn: handler };
                    }

                    // cuando son No terminales 
                    if (Tools.isNotTerminal(symbol)) {
                        this.grammar.firstOnes[symbol].forEach(first => {
                            const that = this;
                            const handler = function (itemOnTop, justText = false) {
                                let text = "";
                                console.log({ itemOnTop, first });
                                
                                let alpha = [...that.grammar.productions
                                    .find(p => (p.leftSide == itemOnTop && Tools.isTerminal(p.definitionItems[0]) ))
                                    .definitionItems.slice(1)]
                                alpha.reverse();

                                if (alpha.length > 0) {
                                    text = `Replace(${alpha}), Avance`;
                                    if (justText) return text;
                                    this.replace(alpha);
                                } else {
                                    text = "Desapile, Avance";
                                    if (justText) return text;
                                    this.pop();
                                }
                            }

                            let text = handler(symbol, true);

                            this.transitions[symbol][first] = { text: text, fn: handler };
                        })
                    }

                    const voidableDirectly = this.grammar.getAvoidableProductions()
                        .filter(obj => obj.production.leftSide === symbol)
                        .length > 0;

                    // cuando es No-terminales y es anulable.
                    if(Tools.isNotTerminal(symbol) && voidableDirectly){
                        this.grammar.nextOnes[symbol].forEach(nextOne => {
                            const that = this;
                            const handler = function (itemOnTop, justText = false) {
                                this.pop();
                                return "KEEP"
                            }
                            const text = "Desapile, Retenga";

                            this.transitions[symbol][nextOne] = { text, fn: handler };
                        })
                    }

                    // primero de las definiciones es NT
                    // el simbolo de entrada pertencece al conjunto de seleccion de la produccion
                    const productionsWithNtAtStart = this.grammar.productions
                        .map((p, i) => ({ index: i, production: p }))
                        .filter(obj => obj.production.leftSide === symbol)
                        .filter(obj => Tools.isNotTerminal( obj.production.definitionItems[0]));
                    
                    productionsWithNtAtStart.forEach(obj => {
                        this.grammar.selectionOnes[""+obj.index].forEach(selItem => {

                            const that = this;
                            const handler = function (itemOnTop, justText = false) {
                                let text = "";
                                let beta = [...that.grammar.productions[obj.index].definitionItems];
                                beta.reverse();

                                text = `Replace(${beta}), Retener`;
                                if (justText) return text;
                                this.replace(beta);
                                return "KEEP";                                
                            }

                            let text = handler(symbol, true);

                            this.transitions[symbol][selItem] = { text: text, fn: handler };

                        })
                    });

                });

                break;
                

            default:
                break;
        }

    }

}

module.exports = StackAutomata;