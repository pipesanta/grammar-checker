const Grammar = require("./Grammar");
const Production = require("./Production");

const ejemploModulo7 = new Grammar([
    new Production("<A>", "a<B><C>"),
    new Production("<A>", "<D>b<A>"),
    new Production("<B>", "-|"),
    new Production("<B>", "b<A><B>"),
    new Production("<C>", "c<C>"),
    new Production("<C>", "<D>d<B>"),
    new Production("<D>", "-|"),
    new Production("<D>", "e<E>"),
    new Production("<E>", "<B><D>"),
    new Production("<E>", "f"),
]);

const modulo11_02 = new Grammar([
    new Production("S", "<A><S><B>"),
    new Production("<S>", "a"),
    new Production("<A>", "a<A>"),
    new Production("<A>", "-|"),
    new Production("<B>", "b<B>"),
    new Production("<B>", "-|"),
]);

const modulo11_03 = new Grammar([
    new Production("S", "<A><B><C><D>"),
    new Production("<A>", "a<A>p"),
    new Production("<A>", "-|"),
    new Production("<B>", "<B>q"),
    new Production("<B>", "b"),
    new Production("<C>", "<A><F>"),
    new Production("<C>", "<C>"),
    new Production("<D>", "d<D>r"),
    new Production("<D>", "-|"),
    new Production("<F>", "f<F>g"),
    new Production("<F>", "-|"),
]);

const modulo13_02 = new Grammar([
    new Production("<P>", "<B>"),
    new Production("<P>", "<CS>"),
    new Production("<B>", "<BH>;<CT>"),
    new Production("<BH>", "bd"),
    new Production("<BH>", "<BH>;d"),
    new Production("<CS>", "b<CT>"),
    new Production("<CT>", "se"),
    new Production("<CT>", "s;<CT>"),
]);

const modulo13_03 = new Grammar([
    new Production("<S>", "a<A>"),
    new Production("<S>", "b<B>"),
    new Production("<A>", "<C><A>1"),
    new Production("<A>", "<C>1"),
    new Production("<B>", "<D><B><E>1"),
    new Production("<B>", "<D><E>1"),
    new Production("<C>", "0"),
    new Production("<D>", "0"),
    new Production("<E>", "1"),
]);

const modulo9_01b = new Grammar([
    new Production("<E>", "<T><listaE>"),
    new Production("<listaE>", "+<T><listaE"),
    new Production("<listaE>", "-|"),
    new Production("<T>", "<P><listaT"),
    new Production("<listaT>", "*<P><listaT>"),
    new Production("<listaT>", "-|"),
    new Production("<P>", "(<E>)"),
    new Production("<P>", "i"),
]);

const modulo14_03 = new Grammar([
    new Production("<S>", "a<S><S>b"),
    new Production("<S>", "d<S><S><S>"),
    new Production("<S>", "c"),
]);

const modulo14_01 = new Grammar([
    new Production("<S>", "a"),
    new Production("<S>", "(<S><R>"),
    new Production("<R>", ",<S><R>"),
    new Production("<R>", ")"),
]);

const modulo15_01 = new Grammar([
    new Production("<S>", "<A><A>d"),
    new Production("<S>", "c<A>d"),
    new Production("<S>", "b"),
    new Production("<A>", "<A><S>c"),
    new Production("<A>", "cd"),
    new Production("<A>", "a"),
    new Production("<A>", "<S>b"),
]);

const modulo16_01 = new Grammar([
    new Production("<P>", "b<D><S>e"),
    new Production("<D>", "<D>d"),
    new Production("<D>", "-|"),
    new Production("<S>", "<S>;<T>"),
    new Production("<S>", "<T>"),
    new Production("<T>", "a"),
    new Production("<T>", "-|"),
]);

const modulo7_05 = new Grammar([
    new Production("<S>", "<A><B><C><D>"),
    new Production("<A>", "a<A>p"),
    new Production("<A>", "-|"),
    new Production("<B>", "<B>q"),
    new Production("<B>", "b"),
    new Production("<C>", "<A><F>"),
    new Production("<D>", "d<D>r"),
    new Production("<D>", "-|"),
    new Production("<F>", "f<F>g"),
    new Production("<F>", "-|"),
]);

const modulo7_04 = new Grammar([
    new Production("<S>", "a<B><D>"),
    new Production("<S>", "<A><B>"),
    new Production("<S>", "<D><A><C>"),
    new Production("<S>", "b"),
    new Production("<A>", "<S><C><B>"),
    new Production("<A>", "<S><A><B><C>"),
    new Production("<A>", "<C>b<D>"),
    new Production("<A>", "c"),
    new Production("<A>", "-|"),
    new Production("<B>", "c<D>"),
    new Production("<B>", "d"),
    new Production("<B>", "-|"),
    new Production("<C>", "<A><D><C>"),
    new Production("<C>", "c"),
    new Production("<D>", "<S>a<C>"),
    new Production("<D>", "<S><C>"),
    new Production("<D>", "fg"),
]);

const modulo11_05 = new Grammar([
    new Production("<A>", "<A>c<B>"),
    new Production("<A>", "c<C>"),
    new Production("<A>", "<C>"),
    new Production("<B>", "b<B>"),
    new Production("<B>", "i"),
    new Production("<C>", "<C>a<B>"),
    new Production("<C>", "<B>b<B>"),
    new Production("<C>", "<B>"),
]);

const gramaticaQ = new Grammar([
    new Production("<A>", "a<C>"),
    new Production("<A>", "b<B>c"),
    new Production("<B>", "a<A>"),
    new Production("<B>", "b<B>"),
    new Production("<C>", "a<C>c"),
    new Production("<C>", "b<B>cc"),
    new Production("<C>", "-|"),
]);

const gramaticaSS = new Grammar([
    new Production("<A>", "a<C>"),
    new Production("<A>", "b<B>c"),
    new Production("<B>", "a<A>"),
    new Production("<B>", "b<B>"),
    new Production("<C>", "a<C>c"),
    new Production("<C>", "b<B>cc"),
    new Production("<C>", "c"),
]);

const gramaticaLL = new Grammar([
    new Production("<A>", "<B>c<D>"),
    new Production("<A>", "a<E>"),
    new Production("<B>", "b<A>c"),
    new Production("<B>", "-|"),
    new Production("<D>", "d<B><D>c"),
    new Production("<D>", "-|"),
    new Production("<E>", "a"),
    new Production("<E>", "<B><D>"),
]);

module.exports = {
    ejemploModulo7,
    modulo11_02,
    modulo11_03,
    modulo13_02,
    modulo13_03,
    modulo9_01b,
    modulo14_03,
    modulo14_01,
    modulo15_01,
    modulo16_01,
    modulo7_05,
    modulo7_04,
    modulo11_05,
    gramaticaQ,
    gramaticaLL,
    gramaticaSS
}


