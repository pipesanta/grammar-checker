import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  Redirect
} from 'react-router-dom';
import MainLayout from './containers/MainLayout';

export default function App() {
  return (
    <Router>
        <Switch>
          <Redirect exact path="/" to="/main" />
          <Route path="/main" render={ (props) => <MainLayout { ...props } /> } >             
          </Route>
          <Route>
            <Redirect to="/main" />
          </Route>
        </Switch>
    </Router>
  );
}