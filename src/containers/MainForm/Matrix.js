import React, { Component } from 'react';

// const data =  [
//     { NT: "<S>", "1": "ACEPTAR" },
//     { NT: "<A>", "0": "REPLACE(<A>bc), CONTINUE()" },
//     { NT: "<D>", "1": "AVANCE"  },
//     { NT: "<F>",  "c": "CONTINUE", X: "SIGA PERRA" },
//     { NT: "<G>", J: "VUEVE A INTENTARLO", "F": "CHEESE TRIX", G: "DETODITOS" }
// ];

const Table = (props) => {
    const { data, style } = props;

    const thtdStyle = {
        border: "1px solid #ddd",
        padding: "8px",
        wordBreak: "keep-all"
    }

    const headerItems = [ 
        "NT",
        ...(data.reduce((acc, v) => {            
            acc.push( ...acc, ...Object.keys(v) );
            acc = [ ...new Set(acc) ];
            return acc;
        }, [])).filter(e => !["-|", "NT"].includes(e)).sort(),
        "-|"
    ];

return (
    <div style={ style } >
        <h1 style={{ "textAlign": "left", "fontFamily": "arial, sans-serif", "fontSize":"30px" }}>Tabla de transiciones</h1>
        <table style={{
             textAlign: "center",
             fontFamily: "Trebuchet MS, Arial, Helvetica, sans-serif",
             borderCollapse: "collapse",
             border: "3px solid #ddd",
             width: "100%"
        }}>
            <tbody>
                {/* HEADER */}
                <tr>{
                    headerItems.map((key, index) => <th key={index} style={thtdStyle} >{key}</th>)
                }</tr>
                {data.map((transition, index) =>(
                       <tr key={index}>{
                            headerItems.map((item, i) => (<td key={i} style={thtdStyle} >{transition[item]}</td>))
                           }
                       </tr>
                    ))
                 }
                 
            </tbody>
        </table>
    </div>
 )
}

export default Table //exporting a component make it reusable and this is the beauty of react