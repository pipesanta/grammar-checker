import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { useHistory } from 'react-router-dom';
import * as Production from '../../components/entities/Production';
import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import AddIcon from '@material-ui/icons/AddCircleOutline';
import Box from '@material-ui/core/Box';
import Matrix from './Matrix';
import Grammar from '../../components/entities/Grammar';
import StackAutomata from '../../components/entities/StackAutomata';
import Recognizer from '../../components/entities/Recognizer';
import gramaticas from '../../components/entities/gramaticas';

const useStyles = makeStyles((theme) => ({
  productionList:{
    "& > .leftSide":{
      marginRight: "12px",
    },
    "& > .rightSide": {
      marginLeft: "12px",
      marginRight: "12px"
    }      
  },
  showTransition: {    
    "& #add_production_btn": {
      marginRight: "20px"
    }
  },
  checkStringSection:{
    "& > .inputText":{
      marginRight: "15px"
    }
  },
  matrixAndtitles:{
    display: "flex",
    flexDirection: "Column",
    "& > .title":{
      fontSize: "20px"
    }
  }
}));

const modulo_8_e_01= new Grammar([
  new Production("<B>", "b<C>d"),
  new Production("<B>", "a<B>"),
  new Production("<C>", "b<C>d"),
  new Production("<C>", "a")
], { auto: true });


const MainGrammarSetup = (props) => {
  const classes = useStyles();

  const [productions, setProductions] = useState(gramaticas.gramaticaQ.productions);
  const [ grammar, SetGrammar ] = useState(gramaticas.gramaticaQ);
  const [ stackAutomata, setStackAutomata ] = useState(null);
  const [ formReadOnly, setFormReadOnly ] = useState(false);
  const [ itemsForMatrix, setItemsForMatrix ] = useState(null);
  const [ stringToValidate, setStringToValidate ] = useState("");
  const [ stringIsValidForGrammar, setStringIsValidForGrammar ] = useState();
  const [ validationSteps, setValidationSteps ] = useState(null);
  const [ errors, setErros ] = useState({});

  const onAddNewProduccionHandler= () => {
    setProductions([...productions, new Production("", "")]);
  }

  const onValidateGrammarHandler= () => {
    setFormReadOnly(true);

    // sync definitionItem on each production
    const updatedProductions  = productions.map(p => {
      p.completeInformation();
      return p;
    });

    setProductions(updatedProductions);


    const currentGrammar = new Grammar(updatedProductions);
    currentGrammar.completeInformation();
    // currentGrammar.print({ firstOnes: true, nextOnes: true });

    SetGrammar(currentGrammar)

    //currentGrammar.type = "S";    
    const newStackAutomata = new StackAutomata(currentGrammar);
    

    newStackAutomata.buildAutomata();
    newStackAutomata.buidTransitionObject();

    console.log(newStackAutomata.transitions);
    console.log(JSON.stringify(newStackAutomata.transitions));

    const transitionsListForMatrix = Object.keys(newStackAutomata.transitions)
      .reduce((acc, key) => {
        const item = { NT: key };

        Object.keys(newStackAutomata.transitions[key]).forEach(sk => {
          item[sk] = newStackAutomata.transitions[key][sk].text;
        });

        acc.push(item);
        return acc;
      }, []);

      // newStackAutomata.buildAutomataS();
      // newStackAutomata.buidTransitionObject();

      newStackAutomata.printInfo()

      setStackAutomata(newStackAutomata);

      setItemsForMatrix(transitionsListForMatrix);

  }

  const onDeleteProduction = (index) => {
    const newProductionList = [ ...productions ];
    newProductionList.splice(index, 1);
    setProductions(newProductionList);

  }

  const onFormChangedHandler = (type, index, event ) => {
    const value = event.target.value;
    console.log({ type, value, index });

    const newProductionList = [...productions];
    const relatedProduction = productions[index];


    if(type === "LEFT_SIDE"){
      // let leftSideParsed = value;
      relatedProduction.leftSide = value.toUpperCase();

    }else if(type === "DEFINITION"){
      relatedProduction.definitionAsString = value;
    }

    relatedProduction.buildDefinitionItems
    newProductionList[index] = relatedProduction;
    setProductions(newProductionList);

  }

  const onLeftSideBlurHandler = (index, event) => {
    // console.log("PONER LOS < > EN EL LADO IZQUIERDO DE LA PRODUCCION NUMERO ", index);
    const newProductionList = [...productions];
    const relatedProduction = productions[index];
    const leftSideItems = relatedProduction.leftSide.trim().split("");
    let leftSideParsed = relatedProduction.leftSide.trim();

    if(!leftSideParsed) return;

    if(leftSideItems[0] !== "<" || leftSideItems[leftSideItems.length-1] !== ">"){
      leftSideParsed = `<${leftSideParsed}>`
    }
    
    newProductionList[index].leftSide = leftSideParsed;
    setProductions(newProductionList);
  }

  const onDefinitionSideBlur = (index) => {
    console.log("poner le simbolo de lamda si el campo esta vacio");

    const newProductionList = [...productions];
    const relatedProduction = productions[index];

    if(!relatedProduction.definitionAsString){
      relatedProduction.definitionAsString = "λ";
    }

    newProductionList[index] = relatedProduction;
    setProductions(newProductionList);


    
  }

  const onStringToValidateChangeHandler = (event) => {
    const value = event.target.value;
    setStringToValidate(value);
  }

  const onValidateTextHandler = () => {

    setValidationSteps(null);
    const reconizer = new Recognizer(stackAutomata);

    stackAutomata.printInfo();

    const { accepted, steps, error } = reconizer.recognizeText(stringToValidate);
    if(error === "end_simbol_missing"){
      setErros({ ...errors, "end_simbol_missing": 'El simbolo de fin de secuencia "-|" es requerido' });
      return;
    }else{ 
      setErros({...error, end_simbol_missing: null})
     }

    setValidationSteps(steps);

    // console.log({ isValidText });
  }

  useEffect(() => {

    console.log("PRODUCCIONES");
    productions.forEach(p => {
      p.print();
    })
  }, [productions])

  

  return (
    <div className="w-full">

      <h1 style={{ "textAlign": "left", "fontFamily": "arial, sans-serif", "fontSize":"30px" }}>
        Producciones de la gramática
      </h1>

      {/* LISTADO DE PRODUCCIONES DE LA GRAMATICA */}
      <div style={{ width: '100%' }} >
      {
        productions.map((p, i) => {
          return (
          <div className="w-full my-8" key={i}>
             <Box className={classes.productionList} display="flex" flexWrap="row">

              <TextField
                className={`leftSide w-2/12`}
                label="No terminal"
                placeholder="No Terminal"
                variant="outlined"
                value={p.leftSide}
                onChange={onFormChangedHandler.bind(this, "LEFT_SIDE", i)}
                onBlur={onLeftSideBlurHandler.bind(this, i)}
                inputProps={{
                  readOnly: formReadOnly
                }}
              />

              <span style={{ "fontSize": "35px"}}> ⇛ </span>
            
              <TextField
                id="outlined-textarea"
                className="rightSide w-8/12"
                label="Definición"
                placeholder="Definición"
                variant="outlined"
                value={p.definitionAsString}
                onChange={onFormChangedHandler.bind(this, "DEFINITION", i)}
                onBlur={onDefinitionSideBlur.bind(this, i)}
                inputProps={{
                  readOnly: formReadOnly
                }}
              />

              <div>
                <IconButton color="secondary" disabled={formReadOnly} aria-label="delete" onClick={onDeleteProduction.bind(this, i)}>
                  <DeleteIcon />
                </IconButton>
              </div>
                

             </Box>
             
          </div>
          )
        })
      }
      </div> 
      {/* LISTADO DE PRODUCCIONES DE LA GRAMATICA */}

      
      

      {/* BOTONES DE ACCIONES */}
      <div className={`${classes.showTransition} w-full flex-row`}  >

      <IconButton id="add_production_btn" disabled={formReadOnly} color="primary" aria-label="add" onClick={onAddNewProduccionHandler}>
        <AddIcon />
      </IconButton>

      <Button className="mx-32" variant="contained" color="primary" onClick={onValidateGrammarHandler}>
        Mostrar transiciones
      </Button>

      </div>
      {/* BOTONES DE ACCIONES */}
      
      { (itemsForMatrix) 
        ? <div className={`${classes.matrixAndtitles}`} >
            <span className="title"> Simbolos en pila: [{stackAutomata.stackSymbols.join(", ")}] </span>
            <span className="title"> Simbolos de entrada: [{stackAutomata.inputSymbols.join(", ")}] </span>
            <span className="title"> Configuración inicial de Pila: [{stackAutomata.initialSetup.join(", ")}] </span>
            { (grammar && grammar.type) 
              ? <span className="title"> La gramatica tipo:  { grammar.type } </span>
              : <span className="title"> { grammar.especificacion() } </span>

            }
            
            <Matrix id="matrix" style={{ marginTop: "12px" }} data={itemsForMatrix} />
          </div>
        : null
      }

      {itemsForMatrix && 
        <h1 style={{ "textAlign": "left", "fontFamily": "arial, sans-serif", "fontSize":"30px", marginTop: "30px" }}>
          Hilera a verificar
        </h1>
      }
      
      {/* HILERA PARA VERIFICAR */}
      { (itemsForMatrix)
        ? <div className="w-full my-8" >

          <Box className={`${classes.checkStringSection}`} display="flex" flexWrap="row">
            
            <TextField
              className= {`inputText w-6/12 m-32`}
              label="No terminal"
              placeholder="No Terminal"
              variant="outlined"
              value={stringToValidate}
              onChange={onStringToValidateChangeHandler}
              // onBlur={onLeftSideBlurHandler.bind(this, i)}
            />
          

            <Button variant="contained" color="primary" onClick={onValidateTextHandler}>
              Validar Hilera
            </Button>

          </Box>
          </div>
        : null

      }
      {
        errors["end_simbol_missing"] && <div> <span style={{ fontSize: "20px", color: "red" }}>
          {errors["end_simbol_missing"]}
        </span> </div>
      }

      {/* { (itemsForMatrix)
        ? <div className="mx-12" ></div>
        : null

      } */}

      {/* RECONIZER RESULT SECTION */}
      { validationSteps && <h1 style={{ "textAlign": "left", "fontFamily": "arial, sans-serif", "fontSize":"30px", marginTop: "30px" }}>
          Verificación paso a paso
        </h1>
      }
      {
        (validationSteps || []).map((step, index) => (

          <div key={index} style={{ 
              fontSize: "20px", 
              color: step.action === "RECHACE" ? "red" : step.action === "ACEPTAR" ? "green" : "black",
              margin: "10px",
              padding: "5px",
              border: "2px solid",
              borderRadius: "15px"
              }} >
            { `Paso ${index+1} STACK: ${step.stack} <<< "${step.input}" || ACTION: ${step.action} `}
            {  step.description ? `  || Descripción: ${step.description}` : "" }
          </div>))
      }

    </div>
    
  );
}

const mapStateToProps = (state) => {
  return {
    token: state.token
  }
}

const mapDispatchToProps = (dispath) => {
  return {

  }
}

export default connect(mapStateToProps, mapDispatchToProps)(MainGrammarSetup);