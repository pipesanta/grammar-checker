import React, {useEffect} from 'react';
import clsx from 'clsx';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from "react-router-dom";
import { useHistory } from 'react-router-dom';
import { connect } from 'react-redux';
import * as actionTypes from '../store/actions';
import { tap, map } from 'rxjs/operators';
import MainForm from './MainForm/MainGrammarSetup'
import ReactNotification from 'react-notifications-component';
import 'react-notifications-component/dist/theme.css';
import { store } from 'react-notifications-component';
import Stack from '../components/entities/Stack';
// import { stringToProductionExpression } from '../components/entities/Tools';


const drawerWidth = 0;

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  appBar: {
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    width: `calc(100% - ${drawerWidth}px)`,
    marginLeft: drawerWidth,
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  hide: {
    display: 'none',
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
  },
  drawerHeader: {
    display: 'flex',
    alignItems: 'center',
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
    justifyContent: 'flex-end',
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    marginLeft: -drawerWidth,
  },
  contentShift: {
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
    marginLeft: 0,
  },
}));

const MainLayout = (props) => {

  const classes = useStyles();
  const theme = useTheme();
  const history = useHistory();

  useEffect(() =>{
      console.log("on init");
      return () => {
          console.log("on destroy");          
      }
  }, []);

  return (
      <div className={classes.root}>
        <ReactNotification />
        <CssBaseline />
        <AppBar
          position="fixed"
          className={clsx(classes.appBar, { [classes.appBarShift]: open })}
        >
          <Toolbar>
            <Typography variant="h6" noWrap>
              Reconocedor de lenguajes
            </Typography>
          </Toolbar>
        </AppBar>
        
        

        {/* BODY */}
        <main className={clsx(classes.content, { [classes.contentShift]: open })} >
          <div className={classes.drawerHeader} />

          <Switch>
            <Redirect exact path="/main" to="/main/configGrammar"/>
            <Route path="/main/configGrammar" render={ (props) => <MainForm { ...props } /> } />
            {/* <Route path="/main/settings"  component={ Setings } />               */}
          </Switch>

        </main>
        {/* BODY */}

      </div>
  );
}

const mapStateToProps = (state) => {
  return {
    token: state.token
  }
}

const mapDispatchToProps = (dispath) => {
  return {
    onSetToken: (token) => dispath({ type: actionTypes.ROOT_SET_TOKEN, token: token })
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(MainLayout);