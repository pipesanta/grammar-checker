const Grammar = require("../src/components/entities/Grammar");
const StackAutomata = require("../src/components/entities/StackAutomata");
const Production = require("../src/components/entities/Production");
const gramaticas = require("../src/components/entities/gramaticas");


function probarLaExtraccionDePrimeros1(){
    const grama = new Grammar([
        new Production("<S>", "a<A><B>b<C><D>"),
        new Production("<S>", "-|"),
        new Production("<A>", "<A><S>d"),
        new Production("<A>", "-|"),
        new Production("<B>", "<S><A>c"),
        new Production("<B>", "e<C>"),
        new Production("<B>", "-|"),
        new Production("<C>", "<S>f"),
        new Production("<C>", "<C>g"),
        new Production("<C>", "-|"),
        new Production("<D>", "a<B><D>"),
        new Production("<D>", "-|")
    ]);
}
// probarLaExtraccionDePrimeros1();

function probarLaExtraccionDePrimeros2(){
    const grama = new Grammar([
        new Production("<A>", "<A>c<B>"),
        new Production("<A>", "c<C>"),
        new Production("<A>", "<C>"),
        new Production("<B>", "b<B>"),
        new Production("<B>", "i"),
        new Production("<C>", "<C>a<B>"),
        new Production("<C>", "<B>b<B>"),
        new Production("<C>", "<B>"),
    ]);

    // console.log("ANULABLES ==> ", grama.noTerminalsAvoidable);
}




// probarLaExtraccionDePrimeros2();


function test3(){
    const grama = new Grammar([
        new Production("<S>", "a<A><B>b<C><D>"),
        new Production("<S>", "-|"),
        new Production("<A>", "<A><S>d"),
        new Production("<A>", "-|"),
        new Production("<B>", "<S><A>c"),
        new Production("<B>", "e<C>"),
        new Production("<B>", "-|"),
        new Production("<C>", "<S>f"),
        new Production("<C>", "<C>g"),
        new Production("<C>", "-|"),
        new Production("<D>", "a<B><D>"),
        new Production("<D>", "-|"),
    ]);    
}

// test3();

function test4(){
    const modulo11_03= new Grammar([
        new Production("P", "<B>"),
        new Production("<P>", "<CS>"),
        new Production("<B>", "<BH>;<CT>"),
        new Production("<BH>", "bd"),
        new Production("<BH>", "<BH>;d"),
        new Production("<CS>", "b<CT>"),
        new Production("<CT>", "se"),
        new Production("<CT>", "s;<CT>"),
    ]);
}
// test4();

function test5(){

    const modulo13_03= new Grammar([
        new Production("<S>", "a<A>"),
        new Production("<S>", "b<B>"),
        new Production("<A>", "<C><A>1"),
        new Production("<A>", "<C>1"),
        new Production("<B>", "<D><B><E>1"),
        new Production("<B>", "<D><E>1"),
        new Production("<C>", "0"),
        new Production("<D>", "0"),
        new Production("<E>", "1"),
    ], { auto: true });

    const stackAutoma = new StackAutomata(modulo13_03);
    modulo13_03.print();

    stackAutoma.buildAutomataS();
    stackAutoma.buidTransitionObject();

}

// test5();


function test6(){
    const modulo9_01b= new Grammar([
        new Production("<E>", "<T><listaE>"),
        new Production("<listaE>", "+<T><listaE"),
        new Production("<listaE>", "-|"),
        new Production("<T>", "<P><listaT"),
        new Production("<listaT>", "*<P><listaT>"),
        new Production("<listaT>", "-|"),
        new Production("<P>", "(<E>)"),
        new Production("<P>", "i"),
    ], { auto: true });

    const stackAutoma = new StackAutomata(modulo9_01b);
    modulo9_01b.print();

    // console.log(modulo9_01b.firstOnesByProductions);
    // console.log(" ==> ", modulo9_01b.getFirstByNoTerminal("<P>"));
    stackAutoma.buildAutomataS();
    stackAutoma.buidTransitionObject();


}



// test6();


function test7(){

    const modulo_7_01= new Grammar([
        new Production("<S>", "<S>c"),
        new Production("<S>", "c<A>"),
        new Production("<S>", "-|"),
        new Production("<A>", "a<A>"),
        new Production("<A>", "a")
    ], { auto: true });


    const stackAutoma = new StackAutomata(modulo_7_01);
    modulo_7_01.print({ firstOnesByProduction: true, firstOnesByNoTerminal: true  });
    

    stackAutoma.buildAutomataS();
    stackAutoma.buidTransitionObject();

}

// test7();


function test8(){
    // pagina 123
    const modulo_8_e_01= new Grammar([
        new Production("<B>", "b<C>d"),
        new Production("<B>", "a<B>"),
        new Production("<C>", "b<C>d"),
        new Production("<C>", "a")
    ], { auto: true });


    const stackAutoma = new StackAutomata(modulo_8_e_01);
    modulo_8_e_01.print({ firstOnesByProduction: true, firstOnesByNoTerminal: true  });
    

    stackAutoma.buildAutomataS();
    stackAutoma.buidTransitionObject();

}

// test8();


const fns =[
    () => {
        const grama = new Grammar(gramaticas.ejemploModulo7.productions);
        grama.completeInformation();
        grama.print({ })
        console.log("anulables ==> ", grama.noTerminalsAvoidable);
        

    }
].forEach(fn => fn());