const Grammar = require("../src/components/entities/Grammar");
const StackAutomata = require("../src/components/entities/StackAutomata");
const Production = require("../src/components/entities/Production");
const gramaticas = require("../src/components/entities/gramaticas");
const Recognizer = require("../src/components/entities/Recognizer");

const grammarsTypeSTestList = [

    () => {

        const modulo_8_e_01= new Grammar([
            new Production("<B>", "b<C>d"),
            new Production("<B>", "a<B>"),
            new Production("<C>", "b<C>d"),
            new Production("<C>", "a")
        ], { auto: true });

        modulo_8_e_01.print();
        console.log("------------", modulo_8_e_01.avoidableProduction);
        
        // modulo_8_e_01.type = "S";
    
        const stackAutomata=new StackAutomata(modulo_8_e_01);


    
        stackAutomata.buildAutomataS();
        stackAutomata.buidTransitionObject();

        // console.log(stackAutomata.transitions);
    
        const reconocedor=new Recognizer(stackAutomata);
    
        const accepted = reconocedor.recognizeText("bbadd-|");
        if(!accepted){
            throw "Este texto debio ser aceptada"
        }
    }

];


const findNextOneTestList = [
    // () => {
    //     const grama= new Grammar([
    //         new Production("<S>", "<A><S><B>"),
    //         new Production("<S>", "a"),
    //         new Production("<A>", "a<A>"),
    //         new Production("<A>", "-|"),
    //         new Production("<B>", "b<B>"),
    //         new Production("<B>", "-|"),
    //     ], { auto: true });

    //     grama.print();
    //     grama.calculateFirstOnes();
    //     console.log(`PRIMEROS POR PROCUCCION`);
        
    //     console.log(grama.firstOnesByProduction)
    //     // const nextList = grama.calculateNextOnes();
    //     // console.log(JSON.stringify(nextList));

    // },
    // () => {
    //     const grama = new Grammar(gramaticas.ejemploModulo7.productions);
    //     grama.completeInformation();
    
    //     grama.print();
    //     grama.isTypeSS();
    //     console.log(grama.noTerminalsAvoidable);
        
    //     console.log("PRIMEROS POR PRODUCCION", grama.firstOnesByProductions)
    // }
    ()=>{
        const grama = new Grammar(gramaticas.gramaticaQ.productions, { auto: true });
        grama.print({ firstOnes: true, nextOnes: true, selectionOnes: true });

    }
];


findNextOneTestList.forEach(fn => fn())


// grammarsTypeSTestList.forEach(fn => fn())

    