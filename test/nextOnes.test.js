const Grammar = require("../src/components/entities/Grammar");
const StackAutomata = require("../src/components/entities/StackAutomata");
const Production = require("../src/components/entities/Production");
const gramaticas = require("../src/components/entities/gramaticas");

const firstFollow = require('first-follow');

const testList = [

    () => {
        const grama = new Grammar(gramaticas.ejemploModulo7.productions, { auto: true });

        // const rules = grama.productions.map(p => ({
        //     left: p.leftSide,
        //     right: p.definitionItems.map(i => i=="-|" ? null : i )
        // }));

        // const { firstSets, followSets, predictSets } = firstFollow(rules);

        grama.print({firstOnes: true, nextOnes: true});
        // console.log({ firstSets, followSets, predictSets  });
        // grama.completeInformation();
    },

    // () => {
    //     const grama = gramaticas.modulo14_03;
    //     grama.print();
    //     grama.completeInformation();
    // },


];

testList.forEach(fn => fn());